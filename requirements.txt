Django==3.2.12
djangorestframework==3.13.1
markdown
django-filter
django-cors-headers==3.10.1
