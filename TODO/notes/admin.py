from django.contrib import admin
from .models import Author, Memo, MemoTag, TagMemoRelationship
# Register your models here.
admin.site.register(Author)
admin.site.register(Memo)
admin.site.register(MemoTag)
admin.site.register(TagMemoRelationship)