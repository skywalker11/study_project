from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST
from .models import Author, Memo, MemoTag, TagMemoRelationship
from .serializers import AuthorSerializer, MemoSerializer, MemoTagSerializer, TagMemoRelationshipSerializer
from typing import List
# Create your views here.

def get_author(request, authorId):
    if authorId.lower() == 'me':
        return next(iter(Author.objects.filter(username = request.user.username)), None)
    return next(iter(Author.objects.filter(id = int(authorId))), None)

class AuthorView(APIView):
    def get(self, request, authorId):
        author = get_author(request, authorId)
        data = AuthorSerializer(author).data
        return Response(data = data)

def parse_tags(tagIds:List[int]):
    if len(tagIds) == 0: return []
    return [tag for tag in MemoTag.objects.all() if tag.id in tagIds]

def update_tags(memo:Memo, tags: List[MemoTag]):
    currentTags = memo.tags
    for curTag in currentTags:
        if not curTag  in tags:
            memo.remove_tag(curTag)
    for tag in tags:
        if not tag in currentTags:
            memo.add_tag(tag)
            

class AuthorMemosView(APIView):
    def get(self, request, authorId):
        author = get_author(request, authorId)
        if author is None:
            return Response(status = HTTP_400_BAD_REQUEST, data = {'detail' : 'author not found'})
        serializer = AuthorSerializer(author)
        return Response(data = {'memos' : serializer.get_memos(author)})
    
    def post(self, request, authorId):
        author = get_author(request, authorId)
        if author is None:
            return Response(status = HTTP_400_BAD_REQUEST, data = {'detail' : 'author not found'})

        memo = request.data
        memo['author'] = author
        serializer = MemoSerializer()
        serializer.create(memo)
        return Response(data = {'updated':True})


class MemoView(APIView):
    def post(self, request, memoId):
        author = get_author(request, 'me') 
        memos = list(author.memos)
        memo = next(iter([memo for memo in memos if memo.id==memoId]))
        
        if memo is None:
            return Response(status = HTTP_400_BAD_REQUEST, data = {'detail' : 'memo not found'})
        
        tags = parse_tags(request.data.get("tags", []))
        update_tags(memo, tags)
        memo.content = request.data.get('content', '')
        memo.save()
        return Response(data = {'updated':True})
    
    def delete(self, request,  memoId):
        author = get_author(request, 'me') 
        memos = list(author.memos)
        memo = next(iter([memo for memo in memos if memo.id==memoId]))
        if not memo is None:
            memo.delete()
            return Response(data = {'updated':True})
        else: Response(data = {'updated':False})

class AuthorViewSet(ModelViewSet):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer


class MemoTagViewSet(ModelViewSet):
    queryset = MemoTag.objects.all()
    serializer_class = MemoTagSerializer


class MemoViewSet(ModelViewSet):
    queryset = Memo.objects.all()
    serializer_class = MemoSerializer

class TagMemoRelationshipSet(ModelViewSet):
    queryset = TagMemoRelationship.objects.all()
    serializer_class = TagMemoRelationshipSerializer